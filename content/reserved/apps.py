from django.apps import AppConfig


class ContentConfig(AppConfig):
    name = 'content'

from suit.apps import DjangoSuitConfig

class SuitConfig(DjangoSuitConfig):
    layout = 'horizontal'