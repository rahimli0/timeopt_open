
# ViewSets define the view behavior.
from django.contrib.auth.models import User
from rest_framework import viewsets, generics
from rest_framework.permissions import IsAuthenticated,AllowAny

from api.serializers import *
from content.models import *


class EmployeeListView(generics.ListAPIView):
    app_pname  = 'employee-list'
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = (AllowAny,)



class EmployeeCreateApiView(generics.CreateAPIView):
    app_pname  = 'employee-create'
    queryset = Employee.objects.filter()
    serializer_class = EmployeeSerializer
    permission_classes = (AllowAny,)

    def perform_create(self, serializer):
        # Note the use of `get_queryset()` instead of `self.queryset`
        serializer.save(user=self.request.user)

class EmployeeRetrieveAPIView(generics.RetrieveAPIView):
    app_pname = 'employee-view'
    queryset = Employee.objects.filter()
    serializer_class = EmployeeSerializer
    permission_classes = (AllowAny,)

class EmployeeUpdateAPIView(generics.RetrieveUpdateAPIView):
    app_pname = 'employee-update'
    queryset = Employee.objects.filter()
    serializer_class = EmployeeSerializer
    permission_classes = (AllowAny,)




class EmployeeDestroyAPIView(generics.RetrieveDestroyAPIView):
    app_pname = 'employee-destroy'
    queryset = Employee.objects.filter()
    serializer_class = EmployeeSerializer
    permission_classes = (AllowAny,)




class LocationListView(generics.ListAPIView):
    app_pname  = 'location-list'
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    permission_classes = (AllowAny,)



class LocationCreateApiView(generics.CreateAPIView):
    app_pname  = 'location-create'
    queryset = Employee.objects.filter()
    serializer_class = LocationSerializer
    permission_classes = (AllowAny,)

    def perform_create(self, serializer):
        # Note the use of `get_queryset()` instead of `self.queryset`
        serializer.save(user=self.request.user)

class LocationRetrieveAPIView(generics.RetrieveAPIView):
    app_pname = 'location-view'
    queryset = Location.objects.filter()
    serializer_class = LocationSerializer
    permission_classes = (AllowAny,)

class LocationUpdateAPIView(generics.RetrieveUpdateAPIView):
    app_pname = 'location-update'
    queryset = Location.objects.filter()
    serializer_class = LocationSerializer
    permission_classes = (AllowAny,)




class LocationDestroyAPIView(generics.RetrieveDestroyAPIView):
    app_pname = 'location-destroy'
    queryset = Location.objects.filter()
    serializer_class = LocationSerializer
    permission_classes = (AllowAny,)


class ExcelDocumentView(generics.ListAPIView):
    app_pname  = 'excel_document-list'
    queryset = ExcelDocument.objects.all()
    serializer_class = ExcelDocumenSerializer
    permission_classes = (AllowAny,)

